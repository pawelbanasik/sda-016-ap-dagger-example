package com.example.rent.sda_016_ap_dagger_example;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.rent.sda_016_ap_dagger_example.model.State;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView goldTextView = (TextView) findViewById(R.id.goldTextView);
        Button balanceButton = (Button) findViewById(R.id.balanceButton);

        StateComponent stateComponent = DaggerStateComponent.create();
        final State state = stateComponent.state();

        goldTextView.setText("gold : "+state.getGold());

        balanceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                state.countBalance();
                goldTextView.setText("gold : "+state.getGold());
            }
        });

    }
}
