package com.example.rent.sda_016_ap_dagger_example;

import com.example.rent.sda_016_ap_dagger_example.model.State;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by RENT on 2017-04-29.
 */
@Singleton
@Component
public interface StateComponent {
    State state();
}
